#!/usr/bin/python3

import sys
import argparse
import logging
import json
import subprocess
import re

log = logging.getLogger("ubuntu-nvidia-tracker-logger")
log.setLevel(logging.INFO)
logging.basicConfig(
    format="%(asctime)s[%(levelname)s]: %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p"
)

'''
Driver
- pkg-name
- transited
- versions
-- drvname
-- name
-- urls
--- updats
--- security
-- archives
--- updats
--- security
-- archs
--- updats
--- security
'''
class DriverVersion:
    pkgname = "" # nvidia-driver-510
    transited = False
    def __init__(self, pkgname):
        self.pkgname = pkgname
        self.pkgver = []
    def new_madison(self, line):
        if not line:
            return
        if line.split(' ')[6] == "Sources":
            return
        self.add_ver(line.split(' ')[2],
                line.split(' ')[4],
                line.split(' ')[5],
                line.split(' ')[6])
    def add_ver(self, ver, url, archive, arch):
        for exist_ver in self.pkgver:
            if exist_ver.name == ver:
                exist_ver.new_source(url, archive, arch)
                return
        nversion = PackageVersion(ver)
        nversion.drvname = self.pkgname
        nversion.new_source(url, archive, arch)
        self.pkgver.append(nversion)
    def dump(self):
        #log.info("Source package: %s" % self.srcname)
        log.info(" - %s:" % self.pkgname)
        for ver in self.pkgver:
            log.info(" > Version: %s" % ver.name)
            for idx, source in enumerate(ver.urls):
                log.info(" >> %s %s %s" % (source, ver.archives[idx], ver.archs[idx]))

class PackageVersion:
    drvname = "" # nvidia-driver-515
    name = "" # 515.48.07-0ubuntu0.20.04.2
    runtimepm_supported = False
    def __init__(self, name):
        self.name = name
        self.urls = [] # http://archive.ubuntu.com/ubuntu
        self.archives = [] # focal-updates/restricted
        self.archs = [] # amd64
    def new_source(self, url, archive, arch):
        self.urls.append(url)
        self.archives.append(archive)
        self.archs.append(arch)

'''
abcd: # GPU()
    nvidia-340: # DriverVersion()
    340.11 # PackageVersion
    340.12
    nvidia-390:
    390.13
    390.14
efgh:
    nvidia-340:
    340.11
    nvidia-390:
    390.15
'''
class GPU():
    modalias = ""
    def __init__(self):
        # stores Class GPU_Version
        self.drivers = []

# which stores GPU() as list:
gpu_list = {}

def is_transited(pkgname):
    cmd_output = subprocess.Popen(
            ['apt', 'show', pkgname],
            stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    while True:
        line = cmd_output.stdout.readline()
        if not line:
            break
        fline = line.rstrip().decode('utf-8')
        if not fline.startswith('Depends:'):
            continue
        if "nvidia-driver-" in fline:
            return True
    return False

def get_nvidia_info():
    drivers = []
    cmd_output = subprocess.Popen(
            ['apt-cache', 'search', '--names-only', 'nvidia'],
            stdout=subprocess.PIPE)
    while True:
        line = cmd_output.stdout.readline()
        if not line:
            break
        pkg_line = line.rstrip().decode('utf-8')
        if pkg_line.startswith("nvidia-driver-"):
            pkgname = pkg_line.split(' ')[0]
            log.info("Searching for %s..." % pkgname)
            driver = DriverVersion(pkgname)
            driver.transited = is_transited(driver.pkgname)
            drivers.append(driver)

    for driver in drivers:
        log.info("Scanning %s ..." % driver.pkgname)
        if driver.transited:
            log.info("... has been transited")
            continue
        cmd_output = subprocess.Popen(
                ['apt-cache', 'madison', driver.pkgname],
                stdout=subprocess.PIPE)
        while True:
            line = cmd_output.stdout.readline()
            if not line:
                break
            pkg_line = line.rstrip().decode('utf-8')
            driver.new_madison(pkg_line)

        for ver in driver.pkgver:
            pkg_ver = "%s=%s" % (driver.pkgname, ver.name)
            cmd_output = subprocess.Popen(
                    ['apt', 'show', pkg_ver],
                    stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
            # Create GPU() object for a specific did
            while True:
                line = cmd_output.stdout.readline()
                if not line:
                    break
                fline = line.rstrip().decode('utf-8')
                if fline.startswith('Modaliases:'):
                    alias_list = re.search('(.*)\((.*)\)(.*)', fline)
                    alias = alias_list.group(2).split(',')
                    for dev in alias:
                        match = re.search('pci:v000010DEd0000(.*)sv(.*)', dev)
                        did = match.group(1)
                        if not did in gpu_list:
                            # new gpu did found
                            gpu = GPU()
                            gpu.modalias = dev
                            gpu.drivers.append(driver)
                            gpu_list[did] = gpu
                        else:
                            drv_added = 0
                            for drv in gpu_list[did].drivers:
                                if drv.pkgname == driver.pkgname:
                                    drv_added = 1
                            if not drv_added:
                                gpu_list[did].drivers.append(driver)
            # TODO: cache it
            cmd_output = subprocess.Popen(
                    ['apt', 'show', pkg_ver],
                    stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
            while True:
                line = cmd_output.stdout.readline()
                if not line:
                    break
                fline = line.rstrip().decode('utf-8')
                if fline.startswith('Pmaliases:'):
                    alias_list = re.search('(.*)\((.*)\)(.*)', fline)
                    alias = alias_list.group(2).split(',')
                    for dev in alias:
                        match = re.search('pci:v000010DEd0000(.*)sv(.*)', dev)
                        did = match.group(1)
                        if not did in gpu_list:
                            log.error("pmaliase %s seems not in modaliase." % dev)
                            break
                        ver.runtimepm_supported = True

if __name__ == "__main__":
    description = """A script to get the nvidia driver status from ubuntu."""
    help = """ubuntu-nvidia-tracker.py -s ${release}"""

    parser = argparse.ArgumentParser(
        description=description,
        epilog=help,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("-s", "--series",
            help="Ubuntu release series, e.g. focal")
    parser.add_argument(
            "-v", "--verbose",
            help="Show more information for debugging.",
            action="store_true", default=False)

    args = parser.parse_args()
    if args.series == None:
        parser.error("Must need to specify a series.")
    if args.verbose:
        log.setLevel(logging.DEBUG)

    get_nvidia_info()

    filename = "ubuntu-%s-supported-nvidia.json" % args.series
    with open(filename, 'w', encoding='utf-8') as f:
        f.write(json.dumps( \
                gpu_list, ensure_ascii=False, default=lambda o: o.__dict__, indent=4))
