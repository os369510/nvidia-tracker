#!/bin/bash
if [ $EUID != 0 ]; then
    echo "Please run as root."
    exit 255
fi

apt update
apt install -y lsb-release python3 python3-pip

pip3 install json

cat <<EOF >/etc/apt/sources.list.d/ubuntu-"$(lsb_release -cs)"-proposed.list
# Enable Ubuntu proposed archive
deb http://archive.ubuntu.com/ubuntu/ $(lsb_release -cs)-proposed restricted main multiverse universe
deb-src http://archive.ubuntu.com/ubuntu/ $(lsb_release -cs)-proposed restricted main multiverse universe
EOF

sed -i 's/# deb-src/deb-src/Ig' /etc/apt/sources.list

apt update
